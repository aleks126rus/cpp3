﻿#include <iostream>
#include <string>
#include <iomanip>

int main()
{
    std::string first = "50003";


    std::cout << "Var = " << first << '\n';
    std::cout << "VarLength = " << first.length() << '\n';
    std::cout << "VarFront = " << first.front() << '\n';
    std::cout << "VarBack = " << first.back() << '\n';
}  
